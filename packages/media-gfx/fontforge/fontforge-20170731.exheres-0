# Copyright 2009-2014 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2
GNULIB_GITHEAD=4612493

require github [ tag=${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require python [ blacklist=none multibuild=false with_opt=true ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="FontForge - An Outline Font Editor"
DESCRIPTION="
FontForge -- An outline font editor that lets you create your own postscript, truetype, opentype,
cid-keyed, multi-master, cff, svg and bitmap (bdf, FON, NFNT) fonts, or edit existing ones. Also
lets you convert one format to another. FontForge has support for many macintosh font formats.
FontForge's user interface has been localized for: (English), Russian, Japanese, French, Italian,
Spanish, Vietnamese, Greek, Simplified & Traditional Chinese, German, Polish, Ukrainian.
"
HOMEPAGE="https://fontforge.github.io"
DOWNLOADS+=" http://git.savannah.gnu.org/gitweb/?p=gnulib.git;a=snapshot;h=${GNULIB_GITHEAD};sf=tgz;name=gnulib-${GNULIB_GITHEAD}.tar.gz -> gnulib-${GNULIB_GITHEAD}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/en-US/documentation [[ lang = en ]]"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    X
    cairo
    collaboration [[ description = [ Collaborative editing ] ]]
    minimal [[ description = [ Only build libraries and python extensions ( if enabled ) ] ]]
    tiff

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/uthash
        sys-devel/gettext
        virtual/pkg-config[>=0.25]
    build+run:
        dev-libs/expat
        dev-libs/glib[>=2.6]
        dev-libs/libffi
        dev-libs/libxml2:2.0
        media-libs/fontconfig
        media-libs/freetype:2[>=2.3.7]
        media-libs/giflib:=
        media-libs/libpng:=[>=1.2]
        media-libs/libuninameslist
        sys-libs/readline:=
        sys-libs/zlib
        x11-libs/harfbuzz
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        X? (
            x11-libs/libICE
            x11-libs/libX11
            x11-libs/libXau
            x11-libs/libXft
            x11-libs/libXi
            x11-libs/libXrender
            x11-libs/libxkbui
            x11-libs/pango[>=1.10][X]
        )
        cairo? ( x11-libs/cairo )
        collaboration? ( net-libs/zeromq[>=4.0.0] )
        tiff? ( media-libs/tiff )
"

# Require inet access
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-20140813-use-system-uthash.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-native-scripting
    --disable-freetype-debugger
    --disable-gtk2-use
    --disable-static
    # dep could also be satified by https://bitbucket.org/sortsmill/libunicodenames/overview
    --with-libuninameslist
    --without-libspiro
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X x'
    cairo
    'collaboration libzmq'
    'tiff libtiff'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    '!minimal programs'
    'python python-scripting'
    'python python-extension'
)

src_prepare() {
    gnulib_modules=( argz warnings manywarnings c-strtod gethostname getline iconv inet_ntop
                     localeconv progname strcase strtok_r strndup xalloc xvasprintf )
    edo ${WORKBASE}/gnulib-${GNULIB_GITHEAD}/gnulib-tool --aux-dir=config --m4-base=m4 --libtool --symlink --import ${gnulib_modules[@]}

    autotools_src_prepare
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

