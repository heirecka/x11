# Copyright 2012 Ivan Dives <ivan.dives@gmail.com>
# Copyright 2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# There's 0.9.14 on github but its build system seems badly broken.
#require github [ user=LibVNC tag=${PV} ]
require sourceforge [ project=libvncserver suffix=tar.gz ]

export_exlib_phases src_install

SUMMARY="a VNC server for real X displays"
DESCRIPTION="
x11vnc allows one to view remotely and interact with real X displays (i.e. a
display corresponding to a physical monitor, keyboard, and mouse) with any VNC
viewer. In this way it plays the role for Unix/X11 that WinVNC plays for Windows.

It has built-in SSL/TLS encryption and 2048 bit RSA authentication, including
VeNCrypt support; UNIX account and password login support; server-side scaling;
single port HTTPS/HTTP+VNC; Zeroconf service advertising; and TightVNC and
UltraVNC file-transfer. It has also been extended to work with non-X devices:
natively on Mac OS X Aqua/Quartz, webcams and TV tuner capture devices, and
embedded Linux systems such as Qtopia Core. Full IPv6 support is provided.

It also provides an encrypted Terminal Services mode (-create, -svc, or -xdmsvc
options) based on Unix usernames and Unix passwords where the user does not need
to memorise his VNC display/port number. Normally a virtual X session (Xvfb) is
created for each user, but it also works with X sessions on physical hardware.
"
LICENCES="GPL-2"
SLOT="0"

REMOTE_IDS+=" freecode:${PN}"

MYOPTIONS="
    avahi
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-libs/libvncserver[>=0.9.1]
        x11-libs/libX11
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXtst
        avahi? ( net-dns/avahi )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=datarootdir
    --hates=docdir
    --with-ipv6
    --with-system-libvncserver
    --with-x
    --with-x11vnc
    --without-client-gcrypt
    --without-client-tls
    --without-crypto
    # It's only used by the ssvnc client (see http://www.karlrunge.com/x11vnc/ssvnc.html
    # for more information) which reason for existence is to provide a ssl tunnel with
    # gui interface (for example on windows). The job that openssl does pretty well.
    --without-client-gcrypt
    --without-ffmpeg
    --without-gcrypt
    --without-gnutls
    --without-macosx-native
    --without-sdl-config
    --without-ssl
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    avahi
)

x11vnc_src_install() {
    default

    edo rmdir \
        "${IMAGE}/usr/$(exhost --target)/include/rfb" \
        "${IMAGE}/usr/$(exhost --target)/include"
}

