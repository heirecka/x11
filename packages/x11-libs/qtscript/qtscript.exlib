# Copyright 2013-2015, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtScript"
DESCRIPTION="Classes for making Qt applications scriptable with ECMAScript. Provided for Qt 4.x
compatibility. For new code the QJS* classes in the QtQml module should be used."

LICENCES+="
    GPL-2
    LGPL-2 [[ note = [ JavaScriptCore ] ]]
"
MYOPTIONS="
    doc examples
    tools    [[ description = [ Build the Qt Script Tools embeddable debugger ] ]]
"

DEPENDENCIES="
    build:
        doc? (
            x11-libs/qttools:${SLOT}[>=5.8.0-rc] [[ note = [ qtattributionsscanner ] ]]
        )
    build+run:
        x11-libs/qtbase:${SLOT}[>=5.5.0][?gui(+)]
        tools? ( x11-libs/qtbase:${SLOT}[gui(+)] )
"

qtscript_src_configure()  {
    # TODO: figure out a way to disable these things sanely
    option tools || edo sed -e "/scripttools/d" -i src/src.pro

    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtscript_src_compile() {
    default

    option doc && emake docs
}

qtscript_src_install() {
    default

    if option doc ; then
        dodoc doc/${PN}.qch
        docinto html
        dodoc -r doc/${PN}
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

